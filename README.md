# Deep Reinforcement Learning Project

This project is inspired by the book "Deep Reinforcement Learning" by [Miguel Morales](https://github.com/mimoralea) (also known as "Grokking Deep Reinforcement Learning").

## References

This project makes use of several grid-world environments and tools. Here are the GitHub references:

- [gym-walk](https://github.com/mimoralea/gym-walk)
- [gym-aima](https://github.com/mimoralea/gym-aima)
- [gym-bandits](https://github.com/mimoralea/gym-bandits)
- [gym toy_text environments from OpenAI](https://github.com/openai/gym/tree/master/gym/envs/toy_text)

## Setting Up the Environment

To set up and run the project, follow these steps:

### Using Conda

1. Ensure you have `conda` installed.
2. Create a new conda environment using the provided `environment.yml` file:
`conda env create -f environment.yml`
3. Activate the newly created environment:
`conda activate your_environment_name`

### Running the Project
Once your environment is set up and activated, you can run the project with:
`python book/run_gridworld.py`