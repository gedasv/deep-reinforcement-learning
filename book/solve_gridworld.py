# solve_gridworld.py
import gym
import my_gridworld.envs.monster_grid_env
import numpy as np


def encode_state(x, y, m1_status, m2_status):
    return x * 8 + y * 4 + m1_status * 2 + m2_status


def decode_state(integer_state):
    m2_status = integer_state % 2
    m1_status = (integer_state // 2) % 2
    y = (integer_state // 4) % 2
    x = (integer_state // 8) % 3
    return (x, y, m1_status, m2_status)


# Policy Evaluation
def policy_evaluation(pi, P, gamma=1.0, theta=1e-10):
    # Initialize the first-iteration estimates of the state-value function to zero
    prev_V = np.zeros(len(P), dtype=np.float64)
    while True:
        # Initialize the current-iteration estimates to zero as well
        V = np.zeros(len(P), dtype=np.float64)
        # Loop trough all states to estimate the state-value function
        for s in range(len(P)):
            for prob, next_state, reward, done in P[s][pi[s]]:
                # Calculate the value of that state by summin up the weighted value of that transition
                V[s] += prob * (reward + gamma * prev_V[next_state] * (not done))
        # We make sure that the state-value functions are changing
        if np.max(np.abs(prev_V - V)) < theta:
            break
        # Finally, "copy" to get ready for the next iteration or return the latest state-value function
        prev_V = V.copy()
    return V


# Policy Improvement
def policy_improvement(V, P, gamma=1.0):
    Q = np.zeros((len(P), len(P[0])), dtype=np.float64)
    for s in range(len(P)):
        for a in range(len(P[s])):
            for prob, next_state, reward, done in P[s][a]:
                Q[s][a] += prob * (reward + gamma * V[next_state] * (not done))
    pi_dict = {s: a for s, a in enumerate(np.argmax(Q, axis=1))}
    return pi_dict


# Policy Iteration
def policy_iteration(P, gamma=1.0, theta=1e-10):
    random_actions = np.random.choice(tuple(P[0].keys()), len(P))
    pi = {s: a for s, a in enumerate(random_actions)}

    while True:
        old_pi = pi.copy()
        V = policy_evaluation(pi, P, gamma, theta)
        pi = policy_improvement(V, P, gamma)

        if old_pi == pi:
            break

    return V, pi


def value_iteration(P, gamma=1.0, theta=1e-10):
    V = np.zeros(len(P), dtype=np.float64)
    while True:
        Q = np.zeros((len(P), len(P[0])), dtype=np.float64)
        for s in range(len(P)):
            for a in range(len(P[s])):
                for prob, next_state, reward, done in P[s][a]:
                    Q[s][a] += prob * (reward + gamma * V[next_state] * (not done))
        if np.max(np.abs(V - np.max(Q, axis=1))) < theta:
            break
        V = np.max(Q, axis=1)

    pi = {s: a for s, a in enumerate(np.argmax(Q, axis=1))}
    return V, pi


def print_policy(
    pi,
    P,
    action_symbols=("UP", "DOWN", "LEFT", "RIGHT", "ATTACK"),
    n_rows=3,
    n_cols=2,
    title="Policy:",
):
    print(title)
    arrs = {k: v for k, v in enumerate(action_symbols)}
    width = len("ATTACK ATTACK")  # The maximum possible width

    for x in range(n_rows):
        print("+", end="")
        for y in range(n_cols):
            print("-" * (width + 2) + "+", end="")
        print("")

        print("|", end="")
        for y in range(n_cols):
            both_alive = arrs[pi[encode_state(x, y, 1, 1)]]
            print(f" {both_alive.center(width)} |", end="")
        print("")

        print("|", end="")
        for y in range(n_cols):
            left_alive = arrs[pi[encode_state(x, y, 1, 0)]]
            right_alive = arrs[pi[encode_state(x, y, 0, 1)]]
            combined = f"{left_alive} {right_alive}".center(width)
            print(f" {combined} |", end="")
        print("")

        print("|", end="")
        for y in range(n_cols):
            both_dead = arrs[pi[encode_state(x, y, 0, 0)]]
            print(f" {both_dead.center(width)} |", end="")
        print("")

    print("+", end="")
    for y in range(n_cols):
        print("-" * (width + 2) + "+", end="")
    print("")


def print_state_value_function(V, P, prec=2):
    """Print the state-value function in a diamond shape."""
    width = max(
        len(str(np.round(val, prec))) for val in V
    )  # The maximum possible width

    for x in range(3):  # 3 rows
        print("+", end="")
        for y in range(2):  # 2 columns
            print("-" * (2 * width + 7) + "+", end="")
        print("")

        # Print both_alive
        for y in range(2):
            both_alive = str(np.round(V[encode_state(x, y, 1, 1)], prec)).center(
                2 * width + 7
            )
            print(f"| {both_alive} ", end="")
        print("|")

        # Print left_alive and right_alive combined
        for y in range(2):  # 2 columns
            left_alive = str(np.round(V[encode_state(x, y, 1, 0)], prec)).ljust(width)
            right_alive = str(np.round(V[encode_state(x, y, 0, 1)], prec)).rjust(width)
            print(f"|    {left_alive} {right_alive}    ", end="")
        print("|")

        # Print both_dead
        for y in range(2):
            both_dead = str(np.round(V[encode_state(x, y, 0, 0)], prec)).center(
                2 * width + 7
            )
            print(f"| {both_dead} ", end="")
        print("|")

    print("+", end="")
    for y in range(2):  # 2 columns
        print("-" * (2 * width + 7) + "+", end="")
    print("")


def random_policy(n_states, n_actions):
    pi = {}
    for s in range(n_states):
        pi[s] = np.random.choice(n_actions)
    return pi


# def probability_success(env, pi, goal_state, n_episodes=100, max_steps=200):
#     random.seed(123); np.random.seed(123) ; env.seed(123)
#     results = []
#     for _ in range(n_episodes):
#         state, done, steps = env.reset(), False, 0
#         while not done and steps < max_steps:
#             state, _, done, h = env.step(pi(state))
#             steps += 1
#         results.append(state == goal_state)
#     return np.sum(results)/len(results)

# def mean_return(env, pi, n_episodes=100, max_steps=200):
#     random.seed(123); np.random.seed(123) ; env.seed(123)
#     results = []
#     for _ in range(n_episodes):
#         state, done, steps = env.reset(), False, 0
#         results.append(0.0)
#         while not done and steps < max_steps:
#             state, reward, done, _ = env.step(pi(state))
#             results[-1] += reward
#             steps += 1
#     return np.mean(results)


def main():
    env = gym.make("MonsterGrid-v0")

    # Extract the MDP
    env.env._create_P()
    P = env.P

    V_best_p, pi_best_p = policy_iteration(P, gamma=0.99)
    # print_state_value_function(V_best_p, P, prec=2)
    # print_policy(pi_best_p, P)

    optimal_V, optimal_pi = value_iteration(P)
    print_state_value_function(optimal_V, P, prec=2)
    print_policy(optimal_pi, P)

    # Print the results
    # print("\nOptimal policy:")
    # print_policy(
    #     optimal_pi, P, action_symbols=("Up", "Down", "Left", "Right", "Attack")
    # )
    # print("\nState-value function:")
    # print_state_value_function(optimal_V, P, n_cols=2)


if __name__ == "__main__":
    main()
