# run_gridworld.py
import gym
import my_gridworld.envs.monster_grid_env

ACTION_NAMES = {
    0: "Move Up",
    1: "Move Down",
    2: "Move Left",
    3: "Move Right",
    4: "Attack Down",
}


def main():
    env = gym.make("MonsterGrid-v0")

    for episode in range(3):  # Do three runs
        print(f"Episode {episode + 1}:")

        state = env.reset()
        done = False
        timestep = 0
        env.render()
        while not done:
            action = env.action_space.sample()  # Random action
            print(f"Iter {timestep + 1}, Taking Action: {ACTION_NAMES[action]}")
            state, reward, done, trunc, info = env.step(action)
            env.render()
            timestep += 1

        print("-------------")


if __name__ == "__main__":
    main()
