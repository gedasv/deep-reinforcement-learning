# my_gridworld/envs/__init__.py
from gym.envs.registration import register

register(
    id="MonsterGrid-v0",
    entry_point="my_gridworld.envs.monster_grid_env:MonsterGridWorldEnv",
    order_enforce=False,
)
