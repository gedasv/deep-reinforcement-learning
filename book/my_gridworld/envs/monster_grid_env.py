# my_gridworld/envs/monster_grid_env.py
import numpy as np
import gym
from gym import spaces
from colorama import Fore, Back, Style, init
from tabulate import tabulate

init(autoreset=True)


def encode_state(x, y, m1_status, m2_status):
    return x * 8 + y * 4 + m1_status * 2 + m2_status


def decode_state(integer_state):
    m2_status = integer_state % 2
    m1_status = (integer_state // 2) % 2
    y = (integer_state // 4) % 2
    x = (integer_state // 8) % 3
    return (x, y, m1_status, m2_status)


class MonsterGridWorldEnv(gym.Env):
    def __init__(self):
        super(MonsterGridWorldEnv, self).__init__()

        # 0: Move Up
        # 1: Move Down
        # 2: Move Left
        # 3: Move Right
        # 4: Attack Down
        # This forms the action set A in MDP.
        self.action_space = spaces.Discrete(5)

        self.monsters = [(2, 0), (1, 1)]
        self.observation_space = spaces.Discrete(24)

        self.goal = (2, 1, 0, 0)
        self.reset()

    def _create_P(self):
        num_actions = 5

        P = {}

        # Iterate through all possible states
        for x in range(3):
            for y in range(2):
                for m1_status in range(2):
                    for m2_status in range(2):
                        state = encode_state(x, y, m1_status, m2_status)
                        P[state] = {action: [] for action in range(num_actions)}

                        # Calculate potential next state for each action
                        for action in range(num_actions):
                            next_x, next_y, next_m1_status, next_m2_status = (
                                x,
                                y,
                                m1_status,
                                m2_status,
                            )

                            if action == 0:  # Move Up
                                next_x = max(next_x - 1, 0)
                            elif action == 1:  # Move Down
                                next_x = min(next_x + 1, 2)
                            elif action == 2:  # Move Left
                                next_y = max(next_y - 1, 0)
                            elif action == 3:  # Move Right
                                next_y = min(next_y + 1, 1)
                            elif action == 4:  # Attack Down
                                if (next_x + 1, next_y) == (
                                    self.monsters[0][0],
                                    self.monsters[0][1],
                                ) and next_m1_status == 1:
                                    next_m1_status = 0
                                    reward = 5
                                elif (next_x + 1, next_y) == (
                                    self.monsters[1][0],
                                    self.monsters[1][1],
                                ) and next_m2_status == 1:
                                    next_m2_status = 0
                                    reward = 5
                                else:
                                    reward = -1

                            next_state = encode_state(
                                next_x,
                                next_y,
                                next_m1_status,
                                next_m2_status,
                            )

                            is_done = False
                            # Determine the reward based on next state
                            if (next_x, next_y) == (
                                self.monsters[0][0],
                                self.monsters[0][1],
                            ) and next_m1_status == 1:
                                reward = -10
                                is_done = True
                            elif (next_x, next_y) == (
                                self.monsters[1][0],
                                self.monsters[1][1],
                            ) and next_m2_status == 1:
                                reward = -10
                                is_done = True
                            elif (
                                next_x,
                                next_y,
                                next_m1_status,
                                next_m2_status,
                            ) == self.goal:
                                reward = 10
                                is_done = True
                            elif action != 4:  # if not an attack action
                                reward = -1

                            # Update P with the transition probabilities
                            P[state][action] = [(1.0, next_state, reward, is_done)]

        self.P = P

    def step(self, action):
        x, y, m1_status, m2_status = decode_state(self.state)

        reward = -1

        # This is the transition function T(s, a, s'). The dynamics are deterministic
        if action == 0:  # up
            x = max(x - 1, 0)
        elif action == 1:  # down
            x = min(x + 1, 2)
        elif action == 2:  # left
            y = max(y - 1, 0)
        elif action == 3:  # right
            y = min(y + 1, 1)
        elif action == 4:  # attack down
            if (x + 1, y) == (
                self.monsters[0][0],
                self.monsters[0][1],
            ) and m1_status == 1:
                m1_status = 0
                reward = 5
            elif (x + 1, y) == (
                self.monsters[1][0],
                self.monsters[1][1],
            ) and m2_status == 1:
                m2_status = 0
                reward = 5

        self.state = encode_state(x, y, m1_status, m2_status)
        # state_index = x * 2 + y

        # Default value indicating episode was not truncated
        info = {"TimeLimit.truncated": False}

        # This is the reward function R(s, a, s'). It gives a scalar reward for each state transition.
        if (x, y) in [self.monsters[0], self.monsters[1]]:
            if (x, y) == self.monsters[0] and m1_status == 1:
                return np.array(self.state), -10, True, False, info
            elif (x, y) == self.monsters[1] and m2_status == 1:
                return np.array(self.state), -10, True, False, info

        if (x, y, m1_status, m2_status) == self.goal:
            return np.array(self.state), 10, True, False, info

        return np.array(self.state), reward, False, False, info

    def reset(self, **kwargs):
        # Reset the environment to its initial state. This is invoked at the start
        # of each new episode.

        # Initial state (initial state distribution). The agent always starts in the top-left corner.
        # In general, D(s) defines the probability the episode starts in state s.
        self.state = encode_state(0, 0, 1, 1)

        return np.array(self.state), {}

    def render(self, mode="human"):
        # Visual representation of the environment. Helps in debugging and understanding agent's behavior.

        x, y, m1_status, m2_status = decode_state(self.state)

        # Initialize the grid with 'o's
        grid = [["o", "o"] for _ in range(3)]

        # Color the agent, monsters, and goal
        grid[x][y] = Fore.BLUE + "S" + Style.RESET_ALL
        grid[self.goal[0]][self.goal[1]] = Fore.YELLOW + "G" + Style.RESET_ALL

        if m1_status == 1:  # if monster at (1,0) is active
            grid[self.monsters[0][0]][self.monsters[0][1]] = (
                Fore.RED + "M" + Style.RESET_ALL
            )
        if m2_status == 1:  # if monster at (1,1) is active
            grid[self.monsters[1][0]][self.monsters[1][1]] = (
                Fore.RED + "M" + Style.RESET_ALL
            )

        # Use tabulate to create a visually appealing grid
        table = tabulate(grid, tablefmt="grid")
        print(table)

    def close(self):
        # Cleanup method, usually used to close any open renderings or other resources.
        pass
